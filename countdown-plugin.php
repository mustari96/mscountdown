<?php
/*
* Plugin Name: MS Countdown
* Description: Shocking Sales Countdown
* Version: 1.0.5
* Author: Mustari Shafiq
* Author URI: https://grobox.com.my
* License: B.O.S
*/

require 'plugin-update-checker-4.11/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/mustari96/mscountdown/',
	__FILE__,
	'wp-ms-countdown'
);
//Optional: If you're using a private repository, specify the access token like this:
// $myUpdateChecker->setAuthentication('your-token-here');

$myUpdateChecker->setBranch('main');  

function get_countdown(){
    
    $html_content = '<div class="countdown-box my-4">'
                    .'<div id="clock-div">'
                        .'<div class="box">'
                            .'<span id="day" class="time-value">0</span>'
                            .'<div class="small-text">HARI</div>'
                        .'</div>'
                        .'<div class="box">'
                            .'<span id="hour" class="time-value">00</span>'
                            .'<div class="small-text">JAM</div>'
                        .'</div>'
                        .'<div class="box">'
                            .'<span id="minute" class="time-value">00</span>'
                            .'<div class="small-text">MINIT</div>'
                        .'</div>'
                        .'<div class="box">'
                            .'<span id="second" class="time-value">00</span>'
                            .'<div class="small-text">SAAT</div>'
                        .'</div>'
                    .'</div>'
                .'</div>';
    
    return $html_content;
}
add_shortcode('bos_countdown', 'get_countdown');

function bos_countdown_headers()
{
    wp_register_style( 'ms_bts', plugins_url( '/bootstrap.css', __FILE__ ), array(), '', 'all' );
    wp_enqueue_style( 'ms_bts' );
    wp_register_style( 'ms_style', plugins_url( '/style.css?v1.2', __FILE__ ), array(), '', 'all' );
    wp_enqueue_style( 'ms_style' );
    wp_register_script( 'ms_jquery', plugins_url( '/jquery.js', __FILE__ ), array(), '', 'all' );
    wp_enqueue_script( 'ms_jquery' );
    wp_register_script( 'ms_custom_jquery', plugins_url( '/custom_jquery.js', __FILE__ ), array(), '', 'all' );
    wp_enqueue_script( 'ms_custom_jquery' );
}

#add hook (name of action hook, name of function)
 add_action( 'wp_enqueue_scripts', 'bos_countdown_headers' );