<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<style>
 /* Countdown CSS */

 .countdown-box {
    display: flex;
    justify-content: center;
}

#clock-div {
    font-family: 'Times New Roman', Times, serif;
    color: #FCFC00;
    display: inline-block;
}

.box {
    background-color: #F34646;
    display: inline-block;
    padding: 15px;
    border-radius: 10px;
    width: 257px;
    height: 112px;
    text-align: center;
    margin: 0px 10px;
}

.small-text {
    font-size: 25px;
    text-align: center;
    line-height: 1.5;
}

#day,
#hour,
#minute,
#second {
    color: #FCFC00;
    font-size: 54px;
    font-weight: 700;
    line-height: 1;
    transform: scale(.5, 2);
}


/* End of Countdown CSS */

@media only screen and (max-width:768px) {
    .box {
        padding: 7px !important;
        width: auto !important;
        height: auto !important;
        margin: 0px 5px !important;
    }

    .time-value {font-size: 45px !important;}

    .small-text {font-size: 1rem !important;}
}

@media only screen and (min-width:769px) and (max-width:1119px) {
    .box {
        padding: 7px !important;
        width: 22vw !important;
        height: auto !important;
        margin: 0px 5px !important;
    }

    .time-value {font-size: 45px !important;}
    
    .small-text {font-size: 1rem !important;}
}
</style>
<div class="countdown-box my-4">
    <div id="clock-div">
        <div class="box">
            <span id="day" class="time-value">0</span>
            <div class="small-text">HARI</div>
        </div>
        <div class="box">
            <span id="hour" class="time-value">00</span>
            <div class="small-text">JAM</div>
        </div>
        <div class="box">
            <span id="minute" class="time-value">00</span>
            <div class="small-text">MINIT</div>
        </div>
        <div class="box">
            <span id="second" class="time-value">00</span>
            <div class="small-text">SAAT</div>
        </div>
    </div>
</div>

<script>
// var endTime = new Date("2 December 2021 0:00:00 GMT+08:00");
// console.log(endTime);

function makeTimer() {

    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow.setHours(0,0,0,0);
    var endTime = new Date(tomorrow);	
    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400); 
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $("#day").html(days);
    $("#hour").html(hours);
    $("#minute").html(minutes);
    $("#second").html(seconds);		

}

setInterval(function() { makeTimer(); }, 1000);
</script>